# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
 str.each_char do |x|
   if x == x.downcase
      str.delete!(x)
   end
 end
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  mid = str.length / 2
  if str.length.even?
    str[mid - 1] + str[mid]
  else
    str[mid]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.each_char.count {|x| VOWELS.include?(x)}
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).inject(:*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
 result = ""
 arr[0..-1].map do |x|
   result += (x + separator)
 end
 if result.length == arr.length
   return result
 else
   return result.chomp(separator)
 end
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") =>
 "wEiRdCaSe"
def weirdcase(str)
  result = []
 str.split("").each_with_index do |x, idx|
  if idx % 2 == 0
    result << x.downcase
  else
    result << x.upcase
  end

 end
 result.join("")
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
 words = str.split(" ")
 result = []
 words.map do |word|
   if word.length >= 5
     result << word.reverse
   else
    result << word
   end
 end
 result.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  result = []
  arr = (1..n).to_a
  arr.each do |int|
    if (int % 5 == 0) && (int % 3 == 0)
      result << "fizzbuzz"
    elsif int % 5 == 0
      result << "buzz"
    elsif int % 3 == 0
      result << "fizz"
    else
      result << int
    end
  end
   result
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  new_arr = []
 arr.reverse.each do |x|
   new_arr << x
 end
 new_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  if num == 1
    return false
  end
  Math.sqrt(num).floor.downto(2).each {|x| return false if num % x == 0}
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  (1..num).select {|i| num % i == 0}
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)

  factors(num).select {|i| prime?(i)}
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odd = []
  even = []
  arr.each do |x|
    if x % 2 == 0
      even << x
    else
      odd << x
    end

  end
  if odd.length == 1
    return odd[0]
  else
    return even[0]
  end
end
